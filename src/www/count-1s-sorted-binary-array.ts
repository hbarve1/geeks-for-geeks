export function method1 (array: number[]): number {
    let count = 0;

    for (let i =0;i<array.length;i+=1){
        if (array[i] === 1){
            count+=1
        }

        if (array[i] ===0 ) {
            return count;
        }
    }

    return count
}


export function method2(array: number[], low: number, high: number): number {
    if (high < low) {
        return 0;
    }

    // get the middle index
    const mid = Math.floor(low + ( high-low) / 2);

    // check if the element at middle index is last 1 
    if ((mid == high || array[mid+1] == 0 ) && (array[mid] ==1)) {
        return mid + 1;
    }

    // if element is not last 1, recur for right side
    if (array[mid] ==1) {
        return method2(array, mid +1, high);
    }

    // else recur the left side
    return method2(array, low, mid-1)
}
