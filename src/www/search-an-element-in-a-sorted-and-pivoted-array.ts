/**
 *
 *
 * @export
 * @param {number[]} array
 * @param {number} value
 * @param {number} low
 * @param {number} high
 * @returns {number}
 */
export function binarySearch(array: number[], value: number, low: number, high: number): number {
  if (low > high) {
    return -1;
  }

  const mid = Math.floor((low + high) / 2);

  if (array[mid] == value) {
    return mid;
  }

  if (array[mid] < value) {
    return binarySearch(array, value, mid + 1, high);
  }
  return binarySearch(array, value, low, mid - 1);
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @param {number} low
 * @param {number} high
 * @returns {number}
 */
export function findPivot(array: number[], low: number, high: number): number {
  if (low > high) return -1;
  if (low == high) return low;

  const mid = Math.floor((low + high) / 2);

  if (mid < high && array[mid] > array[mid + 1]) {
    return mid;
  }

  if (mid > low && array[mid] < array[mid - 1]) {
    return mid - 1;
  }

  if (array[low] >= array[mid]) {
    return findPivot(array, low, mid - 1);
  } else {
    return findPivot(array, mid + 1, high);
  }
}

/**
 *
 *
 * @param {number[]} array
 * @param {number} value
 * @returns {number}
 */
function pivotedBinarySearch(array: number[], value: number): number {
  const pivot = findPivot(array, 0, array.length - 1);

  if (pivot == -1) {
    return binarySearch(array, value, 0, array.length - 1);
  }

  if (array[pivot] == value) {
    return pivot;
  }

  if (array[0] <= value) {
    return binarySearch(array, value, 0, pivot - 1);
  } else {
    return binarySearch(array, value, pivot + 1, array.length - 1);
  }
}

export default pivotedBinarySearch;
