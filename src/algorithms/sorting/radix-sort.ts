/*
  Implement a radix sort in a function called radixSort.

  You can implement it using a binary or decimal based bucketing but I'd recommend the decimal based buckets because
  it ends up being a lot more simple to implement.

  If you need help understanding radix sort, see https://btholt.github.io/four-semesters-of-cs-part-two/radix-sort

  You can visualize each iteration of bucketing and emptying of buckets by calling snapshot(array) at the end of each
  loop. It'll tell you how many iterations you've gone through where it says `Comparisons` at the top.

  You can see what happens with bigger numbers if you change the first unit test to be `xit(...)` and the second unit
  test to be `it`.

*/

function getDigit(number: number, place: number, longestNumber: number): number | 0 {
  const string = number.toString();
  const size = string.length;

  const mod = longestNumber - size;
  return Number(string[place - mod] || 0);
}

function findLongestNumber(array: number[]): number {
  let longest = 0;

  for (let i = 0; i < array.length; i++) {
    const currentLength = array[i].toString().length;

    longest = currentLength > longest ? currentLength : longest;
  }

  return longest;
}

function radixSort(array: number[]): number[] {
  // number with the longest decimal places
  const longestNumber = findLongestNumber(array);

  // make an array of 10 arrays
  // const buckets: number[][] | string[][] = new Array(10)
  //   .fill([])
  //   .map((): number[] | string[] => []);

  const buckets: number[][] = [];

  for (let i = 0; i < 10; i += 1) {
    buckets.push([]);
    for (let j = 0; j < 10; j += 1) {
      buckets.push([]);
    }
  }

  for (let i = longestNumber - 1; i >= 0; i--) {
    while (array.length) {
      const current = array.shift();

      if (current) {
        buckets[getDigit(current, i, longestNumber)].push(current);
      }
    }

    for (let j = 0; j < 10; j++) {
      while (buckets[j].length) {
        const value = buckets[j].shift();

        if (value) {
          array.push(value);
        }
      }
    }
  }

  return array;
}

export default radixSort;
