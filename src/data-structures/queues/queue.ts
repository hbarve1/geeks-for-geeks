interface QueueData {
  [key: number]: number | string;
}

class Queue {
  front: number;
  back: number;
  data: QueueData;

  constructor() {
    this.data = {};
    this.front = 0;
    this.back = 0;
  }

  enqueue(value: number): void {
    this.data[this.front++] = value;
  }

  dequeue(): string | number | null {
    if (this.front <= this.back) {
      return null;
    }

    return this.data[this.back++];
  }

  peek(): number | string {
    return this.data[this.back];
  }
}

export default Queue;
