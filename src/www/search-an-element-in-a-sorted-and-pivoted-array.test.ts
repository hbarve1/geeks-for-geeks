import pivotedBinarySearch from "./search-an-element-in-a-sorted-and-pivoted-array";

describe("search-an-element-in-a-sorted-and-pivoted-array", () => {
  test("Case 1", () => {
    const array = [4, 5, 6, 7, 8, 1, 2, 3];
    const value = 5;

    expect(pivotedBinarySearch(array, value)).toBe(1);
  });

  test("Case 2", () => {
    const array = [1, 2, 3, 4, 5, 6, 7, 8];
    const value = 5;

    expect(pivotedBinarySearch(array, value)).toBe(4);
  });
});
