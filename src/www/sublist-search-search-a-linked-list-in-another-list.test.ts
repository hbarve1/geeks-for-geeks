import subListSearch from "./sublist-search-search-a-linked-list-in-another-list";

describe("sublist-search-search-a-linked-list-in-another-list", () => {
  test("Case 1", () => {
    const list = [5, 10, 20];
    const subList = [10, 20];

    expect(subListSearch(list, subList)).toBe(true);
  });

  test("Case 2", () => {
    const list = [1, 2, 1, 2, 3, 4];
    const subList = [1, 2, 3, 4];

    expect(subListSearch(list, subList)).toBe(true);
  });

  test("Case 3", () => {
    const list = [1, 2, 2, 1, 2, 3];
    const subList = [1, 2, 3, 4];

    expect(subListSearch(list, subList)).toBe(false);
  });

  // test("Case 4", () => {
  //   const list = [];
  //   const subList = [];

  //   expect(subListSearch(list, subList)).toBe(false);
  // });

  // test("Case 5", () => {
  //   const list = [];
  //   const subList = [];

  //   expect(subListSearch(list, subList)).toBe(true);
  // });
});
