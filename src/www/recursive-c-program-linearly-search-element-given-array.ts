/**
 *
 *
 * @param {number[]} array
 * @param {number} value
 * @param {number} [left=0]
 * @param {number} [right=array.length]
 * @returns {number}
 */
function recSearch(
  array: number[],
  value: number,
  left: number = 0,
  right: number = array.length,
): number {
  if (right < left) {
    return -1;
  }

  if (array[left] == value) {
    return left;
  }

  if (array[right] == value) {
    return right;
  }

  return recSearch(array, value, left + 1, right - 1);
}

export default recSearch;
