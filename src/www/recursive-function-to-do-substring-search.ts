/**
 *
 *
 * @export
 * @param {string} string
 * @param {string} pattern
 * @returns {boolean}
 */
export function exactStringMatch(string: string, pattern: string): boolean {
  if (string.length == 0 && pattern.length != 0) {
    return false;
  }

  if (pattern.length == 0) {
    return true;
  }

  if (string[0] == pattern[0]) {
    return exactStringMatch(string.substring(1), pattern.substring(1));
  }

  return false;
}

/**
 *
 *
 * @export
 * @param {string} string
 * @param {string} pattern
 * @returns {boolean}
 */
export function recStringSearch(string: string, pattern: string): boolean {
  if (string.length == 0) {
    return false;
  }

  if (string[0] == pattern[0]) {
    if (exactStringMatch(string, pattern)) {
      return true;
    } else {
      return recStringSearch(string.substring(1), pattern);
    }
  }

  return recStringSearch(string.substring(1), pattern);
}

export default recStringSearch;
