import Queue from "./queue";

describe("Queue", () => {
  let queue: Queue;

  beforeEach(() => {
    queue = new Queue();
  });

  test("Constructor", () => {
    expect(queue).toBeInstanceOf(Queue);

    expect(queue).toHaveProperty("front");
    expect(queue).toHaveProperty("back");
    expect(queue).toHaveProperty("enqueue");
    expect(queue).toHaveProperty("dequeue");
    expect(queue).toHaveProperty("peek");

    expect(queue).not.toHaveProperty("abc");

    expect(queue.front).toBe(0);
    expect(queue.back).toBe(0);
  });

  test("Enqueue", () => {
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    queue.enqueue(4);
    queue.enqueue(5);
    queue.enqueue(6);

    expect(queue.front).toBe(6);
    expect(queue.back).toBe(0);
  });

  test("Dequeue", () => {
    expect(queue.dequeue()).toBe(null);

    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    queue.enqueue(4);
    queue.enqueue(5);
    queue.enqueue(6);

    expect(queue.dequeue()).toBe(1);
    expect(queue.front).toBe(6);
    expect(queue.back).toBe(1);

    expect(queue.dequeue()).toBe(2);
    expect(queue.front).toBe(6);
    expect(queue.back).toBe(2);

    queue.dequeue();
    queue.dequeue();
    queue.dequeue();
    queue.dequeue();

    expect(queue.dequeue()).toBe(null);
  });

  test("Peek", () => {
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    queue.enqueue(4);
    queue.enqueue(5);
    queue.enqueue(6);

    expect(queue.peek()).toBe(1);
    expect(queue.front).toBe(6);
    expect(queue.back).toBe(0);
  });
});
