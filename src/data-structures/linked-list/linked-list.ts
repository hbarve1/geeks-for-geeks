/*
  LinkedList

  Name your class / constructor (something you can call new on) LinkedList

  LinkedList is made by making nodes that have two properties, the value that's being stored and a pointer to
  the next node in the list. The LinkedList then keep track of the head and usually the tail (I would suggest
  keeping track of the tail because it makes pop really easy.) As you may have notice, the unit tests are the
  same as the ArrayList; the interface of the two are exactly the same and should make no difference to the
  consumer of the data structure.

  I would suggest making a second class, a Node class. However that's up to you how you implement it. A Node
  has two properties, value and next.

  length - integer  - How many elements in the list
  push   - function - accepts a value and adds to the end of the list
  pop    - function - removes the last value in the list and returns it
  get    - function - accepts an index and returns the value at that position
  delete - function - accepts an index, removes value from list, collapses,
                      and returns removed value

  As always, you can change describe to xdescribe to prevent the unit tests from running while
  you work
*/

class Node {
  value: string;
  next: Node | null;

  constructor(value: string) {
    this.value = value;
    this.next = null;
  }
}

class LinkedList {
  length: number;
  head: Node | null;
  tail: Node | null;

  constructor() {
    this.length = 0;
    this.head = null;
    this.tail = null;
  }

  /**
   *
   *
   * @returns {string[]}
   * @memberof LinkedList
   */
  serialize(): string[] {
    const ans: string[] = [];
    let current: Node | null = this.head;

    if (!current) return ans;

    while (current) {
      ans.push(current.value);
      current = current.next;
    }

    return ans;
  }

  /**
   *
   *
   * @param {string} value
   * @memberof LinkedList
   */
  push(value: string): void {
    const node = new Node(value);
    this.length++;

    if (!this.head) {
      this.head = node;
    } else if (this.tail) {
      this.tail.next = node;
    }

    this.tail = node;
  }

  /**
   *
   *
   * @returns {(Node | null)}
   * @memberof LinkedList
   */
  findSecondLastNode(): Node | null {
    let head = this.head;

    if (!head) return null;

    if (head == this.tail) return null;

    while (1) {
      if (head.next == this.tail) {
        break;
      }

      if (head.next) {
        head = head.next;
      }
    }

    return head;
  }

  /**
   *
   *
   * @returns {(string | null)}
   * @memberof LinkedList
   */
  pop(): string | null {
    if (!this.head) return null;

    if (this.head === this.tail) {
      const node: Node | null = this.head;
      this.head = this.tail = null;
      this.length--;

      return node.value;
    }

    this.length--;

    const secondLast = this.findSecondLastNode();
    if (secondLast) {
      const last = secondLast.next;

      if (last) {
        const ans = last.value;
        secondLast.next = null;
        this.tail = secondLast;

        return ans;
      }
      return null;
    }
    return null;
  }

  /**
   *
   *
   * @param {number} index
   * @returns {(string | null)}
   * @memberof LinkedList
   */
  get(index: number): string | null {
    // Check if Head is null or not
    if (!this.head) return null;

    // Check if index to be found is greater then the length of LinkedList
    if (this.length < index) return null;

    let pointer: Node = this.head;

    for (let i = 0; i < index; i++) {
      if (pointer.next != null) {
        pointer = pointer.next;
      } else {
        return null;
      }
    }

    return pointer.value;
  }

  /**
   *
   *
   * @param {number} index
   * @returns {(Node | null)}
   * @memberof LinkedList
   */
  findPreviousNode(index: number): Node | null {
    if (index < 1) return null;

    let current = this.head;
    let i = 1;

    while (i < index && current) {
      current = current.next;
      i++;
    }

    return current;
  }

  /**
   *
   *
   * @param {number} index
   * @returns {(string | null)}
   * @memberof LinkedList
   */
  delete(index: number): string | null {
    // Check below condition
    if (index < 0) {
      return null;
    }

    // Check above condition
    if (index > this.length) {
      return null;
    }

    // Check if Head value is to be deleted
    if (index === 0) {
      const head: Node | null = this.head;
      this.length--;

      if (head != null) {
        this.head = head.next;
        return head.value;
      } else {
        this.head = this.tail = null;
        return null;
      }
    }

    const previousNode = this.findPreviousNode(index);
    this.length--;

    if (previousNode) {
      const current = previousNode.next;
      if (!current) return null;

      previousNode.next = current.next;

      if (previousNode.next && !previousNode.next.next) {
        this.tail = previousNode.next;
      }

      return current.value;
    } else {
      return null;
    }
  }
}

export default LinkedList;
