/**
 *
 *
 * @param {number[]} array
 * @param {number} search
 * @returns {number}
 */
function fibonacciSearch(array: number[], search: number): number {
  const length: number = array.length;

  let fibMMm2 = 0; // (m-2)'th
  let fibMMm1 = 1; // (m-1)'th
  let fibM: number = fibMMm1 + fibMMm2; // m'th

  while (fibM < length) {
    fibMMm2 = fibMMm1;
    fibMMm1 = fibM;
    fibM = fibMMm1 + fibMMm2;
  }

  let offset = -1;

  while (fibM > 1) {
    const i: number = Math.min(offset + fibMMm2, length);

    if (array[i] < search) {
      fibM = fibMMm1;
      fibMMm1 = fibMMm2;
      fibMMm2 = fibM - fibMMm1;

      offset = i;
    } else if (array[i] > search) {
      fibM = fibMMm2;
      fibMMm1 = fibMMm1 - fibMMm2;
      fibMMm2 = fibM - fibMMm1;
    } else {
      return i;
    }
  }

  if (fibMMm1 && array[offset + 1] == search) {
    return offset + 1;
  }

  return -1;
}

export default fibonacciSearch;
