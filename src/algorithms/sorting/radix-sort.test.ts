import radixSort from "./radix-sort";
import _ from "lodash";

describe("radix sort", function() {
  test("should sort correctly", () => {
    const input = [
      20,
      51,
      3,
      801,
      415,
      62,
      4,
      17,
      19,
      11,
      1,
      100,
      1244,
      104,
      944,
      854,
      34,
      3000,
      3001,
      1200,
      633,
    ];
    const output = [
      1,
      3,
      4,
      11,
      17,
      19,
      20,
      34,
      51,
      62,
      100,
      104,
      415,
      633,
      801,
      854,
      944,
      1200,
      1244,
      3000,
      3001,
    ];

    const ans = radixSort(input);
    expect(ans).toEqual(output);
  });

  test("should sort correctly 2", () => {
    const count = 99;
    // const nums = new Array(fill).fill().map(() => Math.floor(Math.random() * 500000));

    const numbers: number[] = [];

    for (let i = 0; i < count; i += 1) {
      numbers.push(Math.floor(Math.random() * 500000));
    }

    const ans = radixSort(numbers);

    expect(ans).toEqual(_.sortBy(numbers));
  });
});
