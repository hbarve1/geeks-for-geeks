import Stack from "./stack";

describe("Stack", () => {
  let stack: Stack;

  beforeEach(() => {
    stack = new Stack();
  });

  test("Constructor", () => {
    expect(stack).toBeInstanceOf(Stack);

    expect(stack).toHaveProperty("getData");
    expect(stack).toHaveProperty("push");
    expect(stack).toHaveProperty("pop");
    expect(stack).toHaveProperty("peek");
  });

  test("getData", () => {
    stack.push(1);
    stack.push(1);
    stack.push(1);
    stack.push(1);
    stack.push(1);

    const output = [["0", 1], ["1", 1], ["2", 1], ["3", 1], ["4", 1]];

    expect(stack.getData()).toEqual(output);
  });

  test("Push", () => {
    stack.push(1);
    expect(stack.length).toBe(1);

    stack.push(1);
    expect(stack.length).toBe(2);

    stack.push(1);
    expect(stack.length).toBe(3);

    stack.push(1);
    expect(stack.length).toBe(4);

    stack.push(1);
    expect(stack.length).toBe(5);

    stack.push(1);
    expect(stack.length).toBe(6);
  });

  test("Pop", () => {
    stack.push(1);
    stack.push(4);
    stack.push(5);
    stack.push(12);
    stack.push(18);
    stack.push(20);
    stack.push(80);

    expect(stack.pop()).toBe(80);
    expect(stack.pop()).toBe(20);
  });

  test("Peek", () => {
    stack.push(1);
    stack.push(4);
    stack.push(5);
    stack.push(12);
    stack.push(18);
    stack.push(20);
    stack.push(80);

    expect(stack.peek()).toBe(80);
  });
});
