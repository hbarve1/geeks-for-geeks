/*
  Insertion sort!

  Be sure to call your function insertionSort.

  The idea here is that the beginning of your list is sorted and the everything else is assumed to be an unsorted mess.
  The outer loop goes over the whole list, the index of which signifies where the "sorted" part of the list is. The inner
  loop goes over the sorted part of the list and inserts it into the correct position in the array.
*/

function insertionSort(input: number[]): number[] {
  let array = [...input];
  // console.log(input);

  for (let i = 1; i < array.length; i++) {
    for (let j = 0; j < i; j++) {
      if (array[i] < array[j]) {
        let spliced = array.splice(i, 1);

        // this will add spliced array in location of first argument
        array.splice(j, 0, spliced[0]);

        // console.log({ spliced, array });
      }
    }
  }
  return array;
}

export default insertionSort;
