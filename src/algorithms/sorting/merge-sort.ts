/*
  Write a function that performs mergesort
  Name the function mergeSort
  It will take in a array of numbers and return a sorted array numbers
  
  To read the approach, refer to the class materials at 
  https://btholt.github.io/four-semesters-of-cs/
  
  As always, you can rename describe to be xdescribe to prevent the
  unit tests from running while you're working
  
  There is no visualization mechanism for this algorithm. Use your own
  preferred method of introspection like console.log().
*/

export function merge(leftArray: number[], rightArray: number[]): number[] {
  let newArray: number[] = [];

  while (leftArray.length && rightArray.length) {
    if (leftArray[0] < rightArray[0]) {
      const value = leftArray.shift();
      if (value != undefined) {
        newArray.push(value);
      }
    } else {
      const value = rightArray.shift();
      if (value != undefined) {
        newArray.push(value);
      }
    }
  }

  return newArray.concat(leftArray, rightArray);
}

export function mergeSort(input: number[]): number[] {
  if (input.length < 2) {
    return input;
  }

  const mid = Math.floor(input.length / 2);

  const sortLeft = mergeSort(input.slice(0, mid));
  const sortRight = mergeSort(input.slice(mid));

  return merge(sortLeft, sortRight);
}
