/**
 *
 *
 * @param {number[]} array
 * @param {number} search
 * @param {number} [minIndex=0]
 * @param {number} [maxIndex=array.length - 1]
 * @returns {number}
 */
function binarySearch(
  array: number[],
  search: number,
  minIndex: number = 0,
  maxIndex: number = array.length - 1,
): number {
  const length: number = minIndex + maxIndex;
  const middle: number = Math.floor(length / 2);

  const value: number = array[middle];
  // console.log({ array, minIndex, maxIndex, middle, value });

  if (value === search) {
    return middle;
  }

  if (value > search && middle != minIndex) {
    return binarySearch(array, search, 0, middle - 1);
  }

  if (value < search && middle != maxIndex) {
    return binarySearch(array, search, middle + 1, maxIndex);
  }

  return -1;
}

export default binarySearch;
