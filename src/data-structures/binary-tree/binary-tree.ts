/*

Binary Search Tree!

Name your class Tree.

I'd suggest making another class called Node. You don't have to; you can make them all plain JS objects

Here you'll make a BST. Your Tree class will have keep track of a root which will be the first item added
to your tree. From there, if the item is less than the value of that node, it will go into its left subtree
and if greater it will go to the right subtree.

There is a tree visualization helper. It will tell show you how your tree looks as you create it. In order
for this to work and for the unit tests to pass you, you must implement a Tree .toObject function that returns
your tree as a series of nested objects. Those nested objects must use the following names for their properties

value - integer     - value being contained in the node
left  - Node/object - the left node which itself may be another tree
right - Node/object - the right node which itself may be another tree

As always, you can change describe to xdescribe to prevent the unit tests from running

*/

interface SerializeTree {
  value: number | null;
  left: null | SerializeTree;
  right: null | SerializeTree;
}

class BinaryTreeNode {
  value: number | null;
  left: BinaryTreeNode | null;
  right: BinaryTreeNode | null;
  // serialize: () => SerializeTree;

  constructor(
    value: number | null = null,
    left: BinaryTreeNode | null = null,
    right: BinaryTreeNode | null = null,
  ) {
    this.value = value;
    this.left = left;
    this.right = right;
  }

  serialize(): SerializeTree {
    const ans: SerializeTree = {
      value: this.value,
      left: this.left == null ? null : this.left.serialize(),
      right: this.right == null ? null : this.right.serialize(),
    };

    return ans;
  }
}

class BinaryTree {
  root: BinaryTreeNode | null;

  constructor() {
    this.root = null;
  }

  add(value: number): BinaryTree {
    if (!this.root) {
      this.root = new BinaryTreeNode(value);
    } else {
      let current = this.root;

      while (true) {
        // go left
        if (current.value != null && current.value > value) {
          if (current.left) {
            current = current.left;
          } else {
            current.left = new BinaryTreeNode(value);
            break;
          }
        } else {
          // go right
          if (current.right) {
            current = current.right;
          } else {
            current.right = new BinaryTreeNode(value);
            break;
          }
        }
      }
    }

    return this;
  }

  toJSON(): string {
    const root = this.root;

    return JSON.stringify(root != null ? root.serialize() : null, null, 4);
  }

  toObject(): SerializeTree | null {
    return this.root != null ? this.root.serialize() : null;
  }
}

export default BinaryTree;
