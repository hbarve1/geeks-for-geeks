import AvlTree from "./avl-tree";

describe("AVL Tree", function() {
  it("creates a correct tree", () => {
    const numbers: number[] = [3, 7, 4, 6, 5, 1, 10, 2, 9, 8];
    const tree: AvlTree = new AvlTree();
    expect(tree).toMatchSnapshot();

    let avlTree = tree.toObject();
    expect(avlTree).toBeNull();

    numbers.map(num => tree.add(num));
    avlTree = tree.toObject();
    // render(avlTree, numbers);

    expect(avlTree).toMatchSnapshot();

    expect(avlTree).not.toBeNull();

    // @TODO: Find a better way to test these, for now these are giving errors.
    // expect(avlTree.value).toEqual(4);

    // expect(avlTree.left.value).toEqual(2);

    // expect(avlTree.left.left.value).toEqual(1);
    // expect(avlTree.left.left.left).toBeNull();
    // expect(avlTree.left.left.right).toBeNull();

    // expect(avlTree.left.right.value).toEqual(3);
    // expect(avlTree.left.right.left).toBeNull();
    // expect(avlTree.left.right.right).toBeNull();

    // expect(avlTree.right.value).toEqual(7);

    // expect(avlTree.right.left.value).toEqual(6);
    // expect(avlTree.right.left.right).toBeNull();

    // expect(avlTree.right.left.left.value).toEqual(5);
    // expect(avlTree.right.left.left.left).toBeNull();
    // expect(avlTree.right.left.left.right).toBeNull();

    // expect(avlTree.right.right.value).toEqual(9);

    // expect(avlTree.right.right.left.value).toEqual(8);
    // expect(avlTree.right.right.left.left).toBeNull();
    // expect(avlTree.right.right.left.right).toBeNull();

    // expect(avlTree.right.right.right.value).toEqual(10);
    // expect(avlTree.right.right.right.left).toBeNull();
    // expect(avlTree.right.right.right.right).toBeNull();
  });
});
