/**
 *
 *
 * @class Stack
 */
class Stack {
  data: { [x: string]: number };
  length: number;

  constructor() {
    this.data = {};
    this.length = 0;
  }

  /**
   *
   *
   * @param {number} value
   * @memberof Stack
   */
  push(value: number): void {
    this.data[this.length++] = value;
  }

  /**
   *
   *
   * @returns {number}
   * @memberof Stack
   */
  pop(): number {
    const value = this.data[this.length - 1];
    delete this.data[this.length - 1];

    this.length--;

    return value;
  }

  /**
   *
   *
   * @returns {number}
   * @memberof Stack
   */
  peek(): number {
    return this.data[this.length - 1];
  }

  /**
   *
   *
   * @returns {[string, number][]}
   * @memberof Stack
   */
  getData(): [string, number][] {
    return Object.entries(this.data);
  }
}

export default Stack;
