import recStringSearch from "./recursive-function-to-do-substring-search";

describe("recursive-function-to-do-substring-search", () => {
  test("Case 1", () => {
    const string = "THIS IS A TEST TEXT";
    const pattern = "TEST";

    expect(recStringSearch(string, pattern)).toBe(true);
  });

  test("Case 2", () => {
    const string = "THIS IS A TEST TEXT";
    const pattern = "HELLO";

    expect(recStringSearch(string, pattern)).toBe(false);
  });
});
