import { method1, method2 } from "./two-elements-whose-sum-is-closest-to-zero";

describe("two-elements-whose-sum-is-closest-to-zero", () => {
  describe("Method 1", () => {
    test("Case 1", () => {
      const array = [1, 60, -10, 70, -80, 85];
      const output = [-80, 85];

      expect(method1(array)).toEqual(expect.arrayContaining(output));
    });
  });

  describe("Method 2", () => {
    test("Case 1", () => {
      const array = [1, 60, -10, 70, -80, 85];
      const output = [-80, 85];

      expect(method2(array)).toEqual(expect.arrayContaining(output));
    });
  });
});
