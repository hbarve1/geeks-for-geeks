import { merge, mergeSort } from "./merge-sort";

describe("Merge Two Sorted Array", () => {
  test("Merge One", () => {
    const inputLeft = [2, 4];
    const inputRight = [1, 3];
    const output = [1, 2, 3, 4];

    expect(merge(inputLeft, inputRight)).toEqual(output);
  });

  test("Merge two", () => {
    const inputLeft = [1, 3, 5, 6, 8];
    const inputRight = [2, 4, 7, 9, 10];
    const output = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    expect(merge(inputLeft, inputRight)).toEqual(output);
  });
});

describe("Merge Sort", () => {
  test("Level 1", () => {
    const input = [4, 2, 3, 1];
    const output = [1, 2, 3, 4];

    expect(mergeSort(input)).toEqual(output);
  });
});
