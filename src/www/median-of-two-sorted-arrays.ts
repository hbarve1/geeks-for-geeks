/**
 *
 *
 * @param {number[]} array1
 * @param {number[]} array2
 * @returns {number}
 */
function getMedian(array1: number[], array2: number[]): number {
  let middle1 = -1;
  let middle2 = -1;
  const length: number = array1.length;

  for (let i = 0, j = 0, counter = 0; counter <= length; counter += 1) {
    if (i == length) {
      middle1 = middle2;
      middle2 = array2[0];

      break;
    } else if (j == length) {
      middle1 = middle2;
      middle2 = array1[0];

      break;
    }

    if (array1[i] < array2[j]) {
      middle1 = middle2;
      middle2 = array1[i];

      i++;
    } else {
      middle1 = middle2;
      middle2 = array2[j];

      j++;
    }
  }

  return Math.floor((middle1 + middle2) / 2);
}

export default getMedian;
