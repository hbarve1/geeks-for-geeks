import getMedian from "./median-of-two-sorted-arrays";

describe("median-of-two-sorted-arrays", () => {
  test("Case 1", () => {
    const array1 = [1, 12, 15, 26, 38];
    const array2 = [2, 13, 17, 30, 45];

    expect(getMedian(array1, array2)).toBe(16);
  });
});
