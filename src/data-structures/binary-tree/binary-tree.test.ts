import BinaryTree from "./binary-tree";

describe("Binary Search Tree", function() {
  it("creates a correct tree", () => {
    const numbers: number[] = [3, 7, 4, 6, 5, 1, 10, 2, 9, 8];

    const binaryTree = new BinaryTree();

    let binaryTreeObject = binaryTree.toObject();

    expect(binaryTreeObject).toMatchSnapshot();

    // insert data to tree
    numbers.map(num => binaryTree.add(num));
    binaryTreeObject = binaryTree.toObject();

    expect(binaryTreeObject).toMatchSnapshot();

    // render(binaryTreeObject, numbers);

    // expect(binaryTreeObject.value).toEqual(3);

    // expect(binaryTreeObject.left.value).toEqual(1);
    // expect(binaryTreeObject.left.left).toBeNull();

    // expect(binaryTreeObject.left.right.value).toEqual(2);
    // expect(binaryTreeObject.left.right.left).toBeNull();
    // expect(binaryTreeObject.left.right.right).toBeNull();

    // expect(binaryTreeObject.right.value).toEqual(7);

    // expect(binaryTreeObject.right.left.value).toEqual(4);
    // expect(binaryTreeObject.right.left.left).toBeNull();

    // expect(binaryTreeObject.right.left.right.value).toEqual(6);
    // expect(binaryTreeObject.right.left.right.left.value).toEqual(5);
    // expect(binaryTreeObject.right.left.right.left.right).toBeNull();
    // expect(binaryTreeObject.right.left.right.left.left).toBeNull();

    // expect(binaryTreeObject.right.right.value).toEqual(10);
    // expect(binaryTreeObject.right.right.right).toBeNull();

    // expect(binaryTreeObject.right.right.left.value).toEqual(9);
    // expect(binaryTreeObject.right.right.left.right).toBeNull();

    // expect(binaryTreeObject.right.right.left.left.value).toEqual(8);
    // expect(binaryTreeObject.right.right.left.left.right).toBeNull();
    // expect(binaryTreeObject.right.right.left.left.left).toBeNull();
  });
});
