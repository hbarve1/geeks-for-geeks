import { binarySearch } from "./the-ubiquitous-binary-search-set-1";

describe.skip("Binary Search", () => {
  test("Case 1", () => {
    const array = [10, 20, 30, 50, 60, 80, 100, 110, 130, 170];
    const value = 1;

    expect(binarySearch(array, value)).toBe(-1);
  });

  test("Case 2", () => {
    const array = [10, 20, 30, 50, 60, 80, 100, 110, 130, 170];
    const value = 110;

    expect(binarySearch(array, value)).toBe(7);
  });

  test("Case 3", () => {
    const array = [10, 20, 30, 50, 60, 80, 100, 110, 130, 170];
    const value = 175;

    expect(binarySearch(array, value)).toBe(-1);
  });
});
