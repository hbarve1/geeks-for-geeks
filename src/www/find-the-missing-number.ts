/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {number}
 */
export function method1(array: number[]): number {
  const n = array.length;

  let sum = ((n + 1) * (n + 2)) / 2;

  let result = sum;
  for (let i = 0; i < n; i += 1) {
    result -= array[i];
  }

  return result;
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {number}
 */
export function method2(array: number[]): number {
  const length = array.length;

  let currentXOR = array[0];

  for (let i = 1; i < length; i += 1) {
    currentXOR = currentXOR ^ array[i];
  }

  let expectedXOR = 1;
  for (let i = 2; i <= length + 1; i += 1) {
    expectedXOR = expectedXOR ^ i;
  }

  return currentXOR ^ expectedXOR;
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {number}
 */
export function method3(array: number[]): number {
  let counter = 1;

  for (let i = 0; i < array.length; i += 1) {
    if (counter == array[i]) {
      counter++;
      continue;
    } else {
      return counter;
    }
  }

  return counter;
}
