import mergeSort from "./merge-sort";

/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {number[]}
 */
export function method1(array: number[]): number[] {
  let minValue = Math.abs(array[0] + array[1]);
  let minI = 0;
  let minJ = 0;

  for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array.length; j++) {
      const sum = Math.abs(array[i] + array[j]);

      if (i != j && minValue > sum) {
        minValue = sum;

        minI = i;
        minJ = j;
      }
    }
  }

  return [array[minI], array[minJ]];
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {number[]}
 */
export function method2(array: number[]): number[] {
  array = mergeSort(array);

  let left = 0;
  let minLeft = 0;
  let right = array.length - 1;
  let minRight = right;
  let minSum = Math.abs(array[left] + array[right]);

  while (left < right) {
    let sum = array[left] + array[right];

    if (sum > 0) {
      right--;
    } else if (sum < 0) {
      left++;
    }

    if (minSum > Math.abs(sum)) {
      minSum = sum;
      minLeft = left;
      minRight = right;
    }
  }

  return [array[minLeft], array[minRight]];
}
