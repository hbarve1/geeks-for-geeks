/**
 *
 *
 * @param {number[]} list
 * @param {number[]} subList
 * @returns {boolean}
 */
function subListSearch(list: number[], subList: number[]): boolean {
  const listLength: number = list.length;
  const subListLength: number = subList.length;

  for (let i = 0, j = 0; i < listLength - subListLength + 1; i += 1) {
    if (list[i] !== subList[j]) {
      continue;
    } else if (list[i] === subList[j]) {
      while (i + j < list.length && j < subList.length) {
        if (list[i + j] === subList[j]) {
          if (j === subList.length - 1) {
            return true;
          }

          j++;
        } else {
          j = 0;
          break;
        }
      }
    }
  }

  return false;
}

export default subListSearch;
