/**
 *
 *
 * @param {number[]} array
 * @param {number} low
 * @param {number} high
 * @param {number} search
 * @returns {number}
 */
function getPosition(array: number[], low: number, high: number, search: number): number {
  return Math.floor(low + ((search - array[low]) * (high - low)) / (array[high] - array[low]));
}

/**
 *
 *
 * @param {number[]} array
 * @param {number} search
 * @returns {number}
 */
function interpolationSearch(array: number[], search: number): number {
  let lowIndex = 0;
  let highIndex: number = array.length - 1;

  while (lowIndex <= highIndex && search >= array[lowIndex] && search <= array[highIndex]) {
    if (lowIndex == highIndex) {
      if (array[lowIndex] == search) {
        return lowIndex;
      }

      return -1;
    }

    let position: number = getPosition(array, lowIndex, highIndex, search);

    if (array[position] === search) {
      return position;
    }

    if (array[position] < search) {
      lowIndex = position + 1;
    }

    if (array[position] > search) {
      highIndex = position - 1;
    }
  }

  return -1;
}

export default interpolationSearch;
