class MyArray {
  data: string;

  constructor() {
    this.data = "";
  }

  push(value: string): void {
    this.data += value;
  }

  pop(): string {
    let value = this.data[this.data.length - 1];
    let data = "";

    for (let i = 0; i < this.data.length - 1; i++) {
      data += this.data[i];
    }

    this.data = data;

    return value;
  }

  length(): number {
    return this.data.length;
  }
}

export default MyArray;
