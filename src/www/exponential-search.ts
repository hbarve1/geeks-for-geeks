import binarySearch from "./binary-search";

/**
 *
 *
 * @param {number[]} array
 * @param {number} search
 * @returns {number}
 */
function exponentialSearch(array: number[], search: number): number {
  const length: number = array.length;

  if (search < array[0] || search > array[length - 1]) {
    return -1;
  }

  if (array[0] === search) {
    return 0;
  }

  let i = 1;
  while (i < length && array[i] <= search) {
    i *= 2;
  }

  return binarySearch(array, search, i / 2, Math.min(i, length));
}

export default exponentialSearch;
