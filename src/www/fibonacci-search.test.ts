import fibonacciSearch from "./fibonacci-search";

describe("fibonacci-search", () => {
  test("Case 1", () => {
    const array = [2, 3, 4, 10, 40];
    const search = 10;

    const output = 3;

    expect(fibonacciSearch(array, search)).toBe(output);
  });

  test("Case 2", () => {
    const array = [2, 3, 4, 10, 40];
    const search = 11;

    const output = -1;

    expect(fibonacciSearch(array, search)).toBe(output);
  });

  test("Case 3", () => {
    const array = [10, 22, 35, 40, 45, 50, 80, 82, 85, 90, 100];
    const search = 85;

    const output = 8;

    expect(fibonacciSearch(array, search)).toBe(output);
  });
});
