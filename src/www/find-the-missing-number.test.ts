import { method1, method2, method3 } from "./find-the-missing-number";

describe("find-the-missing-number", () => {
  describe("Method 1", () => {
    test("Case 1", () => {
      const array = [1, 2, 4, 5, 6, 7];

      expect(method1(array)).toBe(3);
    });
  });

  describe("Method 2", () => {
    test("Case 1", () => {
      const array = [1, 2, 4, 5, 6, 7];

      expect(method2(array)).toBe(3);
    });
  });

  describe("Method 3", () => {
    test("Case 1", () => {
      const array = [1, 2, 4, 5, 6, 7];

      expect(method3(array)).toBe(3);
    });
  });
});
