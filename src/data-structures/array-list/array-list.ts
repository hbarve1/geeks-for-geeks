/*
  ArrayList

  We are going to approximate an implementation of ArrayList. In JavaScript terms, that means we are
  going to implement an array using objects. You should not use arrays at all in this exercise, just
  objects. Make a class (or constructor function; something you can call new on) called ArrayList.
  ArrayList should have the following properties (in addition to whatever properties you create):

  length - integer  - How many elements in the array
  push   - function - accepts a value and adds to the end of the list
  pop    - function - removes the last value in the list and returns it
  get    - function - accepts an index and returns the value at that position
  delete - function - accepts an index, removes value from list, collapses,
                      and returns removed value

  As always, you can change describe to xdescribe to prevent the unit tests from running while
  you work
*/

interface ArrayData {
  [key: string]: number | string;
  [key: number]: number | string;
}

class ArrayList {
  length: number;
  data: ArrayData;

  constructor() {
    this.length = 0;
    this.data = {};
  }

  push(value: number | string): void {
    this.data[this.length] = value;
    this.length++;
  }

  pop(): number | string {
    const ans = this.data[this.length - 1];
    delete this.data[this.length - 1];
    this.length--;

    return ans;
  }

  get(index: number | string): number | string {
    return this.data[index];
  }

  delete(index: number): number | string {
    const ans = this.data[index];
    this._collapse(index);
    return ans;
  }

  _collapse(index: number): void {
    for (let i = index; i < this.length; i++) {
      this.data[i] = this.data[i + 1];
    }
    delete this.data[this.length - 1];
    this.length--;
  }
}

export default ArrayList;
