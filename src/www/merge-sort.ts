/**
 *
 *
 * @export
 * @param {number[]} leftArray
 * @param {number[]} rightArray
 * @returns {number[]}
 */
export function merge(leftArray: number[], rightArray: number[]): number[] {
  let newArray: number[] = [];

  while (leftArray.length && rightArray.length) {
    if (leftArray[0] < rightArray[0]) {
      const leftValue = leftArray.shift();
      if (leftValue !== undefined) {
        newArray.push(leftValue);
      }
    } else {
      const rightValue = rightArray.shift();
      if (rightValue !== undefined) {
        newArray.push(rightValue);
      }
    }
  }

  return newArray.concat(leftArray, rightArray);
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {number[]}
 */
export function mergeSort(array: number[]): number[] {
  if (array.length < 2) {
    return array;
  }

  const mid: number = Math.floor(array.length / 2);

  const sortLeft = mergeSort(array.slice(0, mid));
  const sortRight = mergeSort(array.slice(mid));

  return merge(sortLeft, sortRight);
}

export default mergeSort;
