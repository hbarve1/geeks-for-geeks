/**
 *
 *
 * @export
 * @param {number[]} array
 * @param {number} value
 * @returns {number}
 */
export function binarySearch(array: number[], value: number): number {
  let left = 0;
  let right: number = array.length - 1;
  let middle: number;

  while (left <= right) {
    middle = left + (right - 1) / 2;

    if (array[middle] === value) {
      return middle;
    }

    if (array[middle] < value) {
      left = middle + 1;
    } else {
      right = middle - 1;
    }
  }

  return -1;
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @param {number} value
 * @returns {number}
 */
export function binarySearch2(array: number[], value: number): number {
  let left = 0;
  let right: number = array.length;
  let middle: number;

  while (right - left > 1) {
    middle = left + (right - 1) / 2;

    if (array[middle] <= value) {
      left = middle;
    } else {
      right = middle;
    }

    if (array[left] === value) {
      return left;
    } else {
      return -1;
    }
  }

  return -1;
}

/**
 * Problem Statement: Given an array of N distinct integers, find floor value of input
 * ‘key’. Say, A = {-1, 2, 3, 5, 6, 8, 9, 10} and key = 7, we should
 * return 6 as outcome.
 *
 * @export
 * @param {number[]} array
 * @param {number} value
 * @returns {number}
 */
export function findFloorValue(array: number[], value: number): number {
  if (value < array[0]) {
    return -1;
  }

  let left = 0;
  let right: number = array.length;
  let middle: number;

  while (right - left > 1) {
    middle = left + (right - 1) / 2;

    if (array[middle] <= value) {
      left = middle;
    } else {
      right = middle;
    }

    return array[left];
  }

  return array[left];
}

/**
 * Problem Statement: Given a sorted array with possible duplicate elements.
 * Find number of occurrences of input ‘key’ in log N time
 *
 * @export
 * @param {number[]} array
 * @param {number} left
 * @param {number} right
 * @param {number} value
 * @returns {number}
 */
export function getLeftPosition(
  array: number[],
  left: number,
  right: number,
  value: number,
): number {
  let middle: number;

  while (right - left > 1) {
    middle = left + (right - left) / 2;

    if (array[middle] <= value) {
      left = middle;
    } else {
      right = middle;
    }
  }

  return left;
}

/**
 *
 *
 * @param {number[]} array
 * @param {number} left
 * @param {number} right
 * @param {number} value
 * @returns {number}
 */
function getRightPosition(array: number[], left: number, right: number, value: number): number {
  let middle: number;

  while (right - left > 1) {
    middle = left + (right - left) / 2;

    if (array[middle] >= value) {
      right = middle;
    } else {
      left = middle;
    }
  }

  return right;
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @param {number} value
 * @returns {number}
 */
export function countOccurrences(array: number[], value: number): number {
  const left = getLeftPosition(array, -1, array.length - 1, value);
  const right = getRightPosition(array, 0, array.length, value);

  return array[left] === value && array[right] === value ? right - left + 1 : 0;
}

/**
 * Problem Statement: Given a sorted array of distinct elements,
 * and the array is rotated at an unknown position. Find minimum
 * element in the array.
 *
 * @export
 * @param {number[]} array
 * @returns {number}
 */
export function BinarySearchIndexOfMinimumRotatedArray(array: number[]): number {
  let left = 0;
  let right: number = array.length - 1;
  let middle: number;

  if (array[left] <= array[right]) {
    return left;
  }

  while (left < right) {
    if (left == right) {
      return left;
    }

    middle = left + (right - left) / 2;

    if (array[middle] < array[right]) {
      right = middle;
    } else {
      left = middle + left;
    }
  }

  return -1;
}

// @TODO: complete few remaining Exercises
