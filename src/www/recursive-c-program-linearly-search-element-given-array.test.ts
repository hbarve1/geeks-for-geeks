import recSearch from "./recursive-c-program-linearly-search-element-given-array";

describe("Linear Search", () => {
  test("Case 1", () => {
    const array = [10, 20, 80, 30, 60, 50, 110, 100, 130, 170];
    const search = 110;

    expect(recSearch(array, search)).toBe(6);
  });

  test("Case 2", () => {
    const array = [10, 20, 80, 30, 60, 50, 110, 100, 130, 170];
    const search = 175;

    expect(recSearch(array, search)).toBe(-1);
  });
});
