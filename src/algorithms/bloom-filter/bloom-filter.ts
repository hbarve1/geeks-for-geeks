import XXH from "xxhashjs";
const BASE_NUMBER = 100;

// here are your hashing functions. it's not essential you know how they work
// a library called xxhashjs is being loaded (as XXH) and we're using three different
// instances of that as your hashing functions
const h1 = (string: string): number =>
  Math.abs(
    XXH.h32(0xabcd)
      .update(string)
      .digest()
      .toNumber() % BASE_NUMBER,
  );
const h2 = (string: string): number =>
  Math.abs(
    XXH.h32(0x1234)
      .update(string)
      .digest()
      .toNumber() % BASE_NUMBER,
  );
const h3 = (string: string): number =>
  Math.abs(
    XXH.h32(0x6789)
      .update(string)
      .digest()
      .toNumber() % BASE_NUMBER,
  );

class BloomFilter {
  _array: number[];

  constructor() {
    this._array = new Array(BASE_NUMBER).fill(0);
  }

  // `add` adds a string to the bloom filter and returns void (nothing, undefined)
  add(string: string): void {
    this._array[h1(string)] = 1;
    this._array[h2(string)] = 1;
    this._array[h3(string)] = 1;
  }

  // `contains` takes a string and tells you if a string is maybe in the bloom filter
  contains(string: string): boolean {
    return !!(this._array[h1(string)] && this._array[h2(string)] && this._array[h3(string)]);
  }
}

export default BloomFilter;
