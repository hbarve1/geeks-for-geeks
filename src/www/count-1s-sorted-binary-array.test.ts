import { method1, method2} from './count-1s-sorted-binary-array'

// NOTE: array is sorted, 1's are in front.

describe('count-1s-sorted-binary-array', () => {
    test('method 1', () => {
        expect(method1([1])).toBe(1)
        expect(method1([0])).toBe(0)
        expect(method1([1, 1, 1, 0, 0, 0])).toBe(3)
        expect(method1([1, 1, 1, 1, 0, 0])).toBe(4)
    })

    test('method 2', () => {
        expect(method2([1], 0, 0)).toBe(1)
        expect(method2([0], 0, 0)).toBe(0)
        expect(method2([1, 1, 1, 1, 1, 0, 0, 0], 0, 7)).toBe(5)
        expect(method2([1, 1, 0, 0, 0, 0, 0, 0], 0, 7)).toBe(2)
    })
})
