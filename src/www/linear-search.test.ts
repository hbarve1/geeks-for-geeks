import linearSearch from "./linear-search";

describe("Linear Search", () => {
  test("Test 1", () => {
    const array = [10, 20, 80, 30, 60, 50, 110, 100, 130, 170];
    const search = 110;

    expect(linearSearch(array, search)).toBe(6);
  });

  test("Test 2", () => {
    const array = [10, 20, 80, 30, 60, 50, 110, 100, 130, 170];
    const search = 175;

    expect(linearSearch(array, search)).toBe(-1);
  });
});
