/*
  AVL Tree

  Name you class/function (anything we can call new on) Tree

  I would suggest making a Node class as well (it will help _a lot_ with AVL trees) Whereas with BSTs we
  could get away with most of the logic living in the Tree class, that will be a lot tougher with AVL
  trees dues how the function calls must be recursive in order to get the balancing correct.

  Tree must a method called add that takes a value and adds it to the tree and then correctly balances the
  tree. There is only one correct structure for any given order of adding numbers and the unit tests enforce
  that structure.

  If you have any questions conceptually about balancing the tree, refer to the class website.

  There is a tree visualization engine that should run automatically. Make sure you are calling the properties
  of the Nodes as follows:
  value - integer - the value being store in the tree
  left  - Node    - the subtree containing Node's with values less than the current Node's value
  right - Node    - the subtree containing Node's with values greater than the current Node's value

  As always, you can rename describe to xdescribe to prevent the unit tests from running and the visualization
  from displaying

*/

export interface SerializeResponse {
  value: number | null;
  left: SerializeResponse | null;
  right: SerializeResponse | null;
  height: number;
}

export class AvlNode {
  value: number | null;
  left: AvlNode | null;
  right: AvlNode | null;
  height: number;

  constructor(
    value: number | null = null,
    left: AvlNode | null = null,
    right: AvlNode | null = null,
  ) {
    this.value = value;
    this.left = left;
    this.right = right;
    this.height = 1;
  }

  /**
   *
   *
   * @param {number} value
   * @memberof AvlNode
   */
  add(value: number): void {
    if (this.value != null && value < this.value) {
      // go left

      if (this.left) {
        this.left.add(value);
      } else {
        this.left = new AvlNode(value);
      }

      if (!this.right || this.right.height < this.left.height) {
        this.height = this.left.height + 1;
      }
    } else {
      // go right

      if (this.right) {
        this.right.add(value);
      } else {
        this.right = new AvlNode(value);
      }

      if (!this.left || this.left.height < this.right.height) {
        this.height = this.right.height + 1;
      }
    }

    this.balance();
  }

  balance(): void {
    const rightHeight: number = this.right ? this.right.height : 0;
    const leftHeight: number = this.left ? this.left.height : 0;

    if (leftHeight > rightHeight + 1) {
      const leftRightHeight = this.left && this.left.right ? this.left.right.height : 0;
      const leftLeftHeight = this.left && this.left.left ? this.left.left.height : 0;

      if (leftRightHeight > leftLeftHeight && this.left) {
        this.left.rotateRR();
      }

      this.rotateLL();
    } else if (rightHeight > leftHeight + 1) {
      const rightRightHeight = this.right && this.right.right ? this.right.right.height : 0;
      const rightLeftHeight = this.right && this.right.left ? this.right.left.height : 0;

      if (rightLeftHeight > rightRightHeight && this.right) {
        this.right.rotateLL();
      }

      this.rotateRR();
    }
  }

  rotateRR(): void {
    const valueBefore = this.value;
    const leftBefore = this.left;

    this.value = this.right ? this.right.value : null;
    this.left = this.right;
    this.right = this.right ? this.right.right : null;

    if (this.left) {
      this.left.right = this.left ? this.left.left : null;
      this.left.left = leftBefore;
      this.left.value = valueBefore;
      this.left.updateInNewLocation();

      this.updateInNewLocation();
    }
  }

  rotateLL(): void {
    const valueBefore = this.value;
    const rightBefore = this.right;

    this.value = this.left ? this.left.value : null;
    this.right = this.left;
    this.left = this.left ? this.left.left : null;

    if (this.right) {
      this.right.left = this.right ? this.right.right : null;
      this.right.right = rightBefore;
      this.right.value = valueBefore;
      this.right.updateInNewLocation();

      this.updateInNewLocation();
    }
  }

  updateInNewLocation(): void {
    if (!this.right && !this.left) {
      this.height = 1;
    } else if (!this.right || (this.left && this.right.height < this.left.height)) {
      this.height = this.left ? this.left.height + 1 : 0;
    } else {
      // if(!this.left || this.left.height < this.right.height)
      this.height = this.right.height + 1;
    }
  }

  serialize(): SerializeResponse {
    const ans = {
      value: this.value,
      left: this.left === null ? null : this.left.serialize(),
      right: this.right === null ? null : this.right.serialize(),
      height: this.height,
    };

    return ans;
  }
}

class AvlTree {
  root: AvlNode | null;

  constructor() {
    this.root = null;
  }

  add(value: number): void {
    if (!this.root) {
      this.root = new AvlNode(value);
    } else {
      this.root.add(value);
    }
  }

  toJSON(): string {
    const root: AvlNode | null = this.root;

    return JSON.stringify(root != null ? root.serialize() : null, null, 4);
  }

  toObject(): SerializeResponse | null {
    const root = this.root;

    return root != null ? root.serialize() : null;
  }
}

export default AvlTree;
