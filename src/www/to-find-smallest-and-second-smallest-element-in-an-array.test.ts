import {
  method1,
  method2,
  method3,
} from "./to-find-smallest-and-second-smallest-element-in-an-array";

describe("to-find-smallest-and-second-smallest-element-in-an-array", () => {
  describe("Method 1", () => {
    test("Case 1", () => {
      const array = [12, 13, 1, 10, 34, 1];
      const output = {
        smallest: 1,
        secondSmallest: 10,
      };

      expect(method1(array)).toMatchObject(output);
    });
  });

  describe("Method 2", () => {
    test("Case 1", () => {
      const array = [12, 13, 1, 10, 34, 1];
      const output = {
        smallest: 1,
        secondSmallest: 10,
      };

      expect(method2(array)).toMatchObject(output);
    });
  });

  describe("Method 3", () => {
    test("Case 1", () => {
      const array = [12, 13, 1, 10, 34, 1];
      const output = {
        smallest: 1,
        secondSmallest: 10,
      };

      expect(method3(array)).toMatchObject(output);
    });
  });
});
