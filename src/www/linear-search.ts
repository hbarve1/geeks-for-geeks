/**
 *
 *
 * @param {number[]} array
 * @param {number} search
 * @returns {number}
 */
function linearSearch(array: number[], search: number): number {
  for (let i = 0; i < array.length; i++) {
    if (array[i] === search) return i;
  }

  return -1;
}

export default linearSearch;
