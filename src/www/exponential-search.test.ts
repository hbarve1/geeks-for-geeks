import exponentialSearch from "./exponential-search";

describe("Exponential Search", () => {
  test("Case 1", () => {
    const array = [10, 20, 30, 50, 60, 80, 100, 110, 130, 170];
    const search = 1;

    expect(exponentialSearch(array, search)).toBe(-1);
  });

  test("Case 2", () => {
    const array = [10, 20, 30, 50, 60, 80, 100, 110, 130, 170];
    const search = 110;

    expect(exponentialSearch(array, search)).toBe(7);
  });

  test("Case 3", () => {
    const array = [10, 20, 30, 50, 60, 80, 100, 110, 130, 170];
    const search = 175;

    expect(exponentialSearch(array, search)).toBe(-1);
  });
});
