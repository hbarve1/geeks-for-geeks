import { mergeSort } from "./merge-sort";

interface Response {
  smallest: number;
  secondSmallest: number;
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {Response}
 */
export function method1(array: number[]): Response {
  array = mergeSort(array);
  const smallest = array[0];
  let secondSmallest = array[0];

  for (let i = 0; i < array.length; i++) {
    if (smallest < array[i]) {
      secondSmallest = array[i];
      break;
    }
  }

  return {
    smallest,
    secondSmallest,
  };
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {Response}
 */
export function method2(array: number[]): Response {
  let smallest = array[0];
  let secondSmallest = array[0];

  for (let i = 0; i < array.length; i++) {
    if (array[i] < smallest) {
      smallest = array[i];
    }
  }

  for (let i = 0; i < array.length; i++) {
    if (array[i] > smallest && array[i] < secondSmallest) {
      secondSmallest = array[i];
    }
  }

  return {
    smallest,
    secondSmallest,
  };
}

/**
 *
 *
 * @export
 * @param {number[]} array
 * @returns {Response}
 */
export function method3(array: number[]): Response {
  let smallest: number = array[0];
  let secondSmallest: number = array[0];

  for (let i = 0; i < array.length; i++) {
    if (array[i] < smallest) {
      smallest = array[i];
    }

    if (array[i] < secondSmallest && array[i] > smallest) {
      secondSmallest = array[i];
    }
  }

  return {
    smallest,
    secondSmallest,
  };
}
