/**
 *
 *
 * @param {number[]} array
 * @param {number} search
 * @returns {number}
 */
function jumpSearch(array: number[], search: number): number {
  const length: number = array.length;
  let step: number = Math.floor(Math.sqrt(length));

  // first find the block in which element can exist
  let minIndex = 0;

  while (array[Math.min(minIndex, step) - 1] < search) {
    minIndex = step;
    step += Math.floor(Math.sqrt(length));

    if (minIndex >= length) {
      return -1;
    }
  }

  // do linear search to find element in that block
  for (let i = minIndex; i <= length; i++) {
    if (array[i] === search) return i;
  }

  // if no found in given block, element does not exist
  return -1;
}

export default jumpSearch;
